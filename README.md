# OpenML dataset: scene

https://www.openml.org/d/312

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Matthew R. Boutell, Jiebo Luo, Xipeng Shen, and Christopher M. Brown.  
**Source**: [Mulan](http://mulan.sourceforge.net/datasets-mlc.html)     
**Please cite**: 

### Description

Scene recognition dataset - It contains characteristics about images and their classes. 
The original dataset is a multi-label classification problem with 6 different labels: {Beach, Sunset, FallFoliage, Field, Mountain, Urban}.
The current dataset is a binary classification problem considering just the 'Urban' label.

### Sources

Matthew R. Boutell, Jiebo Luo, Xipeng Shen, and Christopher M. Brown.
Learning multi-label scene classification.
Pattern Recognition, 37(9):1757-1771, 2004. 

### Dataset Information

Multi-label classification problem, based on real-world images.   
Instances: 2407    
Features: 294 numerical features with values between [0,1]   
Classes/Labels: 2 {Urban, Nor Urban}   
No missing values

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/312) of an [OpenML dataset](https://www.openml.org/d/312). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/312/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/312/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/312/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

